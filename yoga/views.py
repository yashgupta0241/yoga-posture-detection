from django.shortcuts import render
from .models import TutorialVideo
# Create your views here.
def index(request):
    return render(request,'yoga/index.html')

def classes(request):
    videos = TutorialVideo.objects.all()
    firstvideo = videos.first()
    return render(request, 'yoga/classes.html', {'firstvideo':firstvideo,'videos':videos})