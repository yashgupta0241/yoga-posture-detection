from django.urls import path
from . import views

urlpatterns = [ 
    path('', views.index, name='Homepage'),
    path('classes/', views.classes, name='classes')
] 