# Generated by Django 3.2.12 on 2022-08-03 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('yoga', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutorialvideo',
            name='url',
            field=models.CharField(max_length=200),
        ),
    ]
