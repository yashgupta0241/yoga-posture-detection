from unittest.util import _MAX_LENGTH
from django.db import models
import datetime
from django.utils.timezone import now
import os

# Create your models here.
def filepath(request, filename):
    old_filename = filename
    timeNow = datetime.datetime.now().strftime('%Y%m%d%H:%M:%S')
    filename = "%s%s" % (timeNow, old_filename)
    return os.path.join('uploads/', filename)

class TutorialVideo(models.Model):
    name = models.CharField(max_length=1000,default=None)
    url = models.CharField(max_length=200)
    image = models.ImageField(upload_to=filepath)
    created_at = models.DateTimeField(default=now)